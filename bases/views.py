
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic
from logging import raiseExceptions
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic.base import TemplateView
from django.contrib import messages
from django.shortcuts import render, redirect
from apoyos.models import Persona, Apoyos, EncargadoRuta, Empleado, Departamento, Localidad, Puesto
from catastro.models import Contribuyente
from datetime import datetime

# Create your views here.
class Sin_Privilegios(LoginRequiredMixin, PermissionRequiredMixin):
    login_url = 'bases:login'
    raiseExceptions = False # no se ponga la pantalla en blanco
    redirect_field_name = "redirect_to"

    def handle_no_permission(self):
        """ método que valida si el usuario tiene privilegios, para redireccionar a plantilla"""
        from django.contrib.auth.models import AnonymousUser

        # si el usuario es válido, se muestra la vista sin privilegios
        if not self.request.user == AnonymousUser():
            self.login_url = "bases:sin_privilegios"

        return HttpResponseRedirect(reverse_lazy(self.login_url))
    
class Home(LoginRequiredMixin, TemplateView):    
    template_name = 'bases/home.html'
    login_url = 'bases:login' # sino esta logeado se redireccona al admin

class HomeSinPrivilegios(LoginRequiredMixin, generic.TemplateView):
    login_url = 'bases:login'
    template_name = "bases/sin_privilegios.html"
        
def getEstadisticas(request):
    """ mostrar todos los activistas"""
    # total de personas
    personas = Persona.objects.all().count()
    
    # total de activistas
    activistas = Persona.objects.all().filter(tipo="ACTIVISTA").count()
    # print(activistas)
    
    # total de activados
    activados = Persona.objects.all().filter(tipo="ACTIVADO").count()
    # print(activados)
    
    # total de apoyos entregados
    apoyos = Apoyos.objects.all().count()
    # total de encargados de ruta
    encargados = EncargadoRuta.objects.all().count()
    # total de empleados
    empleados = Empleado.objects.all().count()
    # total de departamentos
    departamentos = Departamento.objects.all().count()
    # total de localidades
    localidades = Localidad.objects.all().count()
    # total de puestos
    puestos = Puesto.objects.all().count()
    
    # total de contribuyentes
    contribuyentes = Contribuyente.objects.all().count()
    
    template_name = "bases/home.html"
    context = {
        'personas': personas,
        'activistas': activistas,
        'activados': activados,
        'apoyos': apoyos,
        'encargados': encargados,
        'empleados': empleados,
        'departamentos': departamentos,
        'localidades': localidades,
        'contribuyentes': contribuyentes, 
        'puestos': puestos
    }
    
    return render(request, template_name, context)

def menu_de_personas(request):
    template_name = "bases/menu_personas.html"   
    context = {}
    
    return render(request, template_name, context)

def get_personas_por_curp(request):
    """ consultamos personas por CURP"""    
    # extraemos el valor del input
    curp = request.GET.get("curp")
    curp = str(curp)
    # print(curp)
    persona = Persona.objects.filter(curp=curp).first()
    
    # print(persona)
    # print(persona.id)
    template_name = "bases/busqueda-curp.html"
    context = {'obj': persona}    

    return render(request, template_name, context)

def get_personas_por_nombre(request):
    """ consultamos personas por nombre completo"""    
    # extraemos el valor del input nombre
    nombre = request.GET.get("nombre")
    nombre = str(nombre)    
    # eliminamos espacios en blanco del inicio y final
    nombre = nombre.strip() 
    nombre = nombre.upper()
    # print(type(nombre))
    # print(nombre)    
    
    # extraemos el valor del input ap paterno
    ap_paterno = request.GET.get("apellido_paterno")
    ap_paterno = str(ap_paterno)
    ap_paterno = ap_paterno.strip()
    ap_paterno = ap_paterno.upper()
    
    # extraemos el valor del input ap materno
    ap_materno = request.GET.get("apellido_materno")
    ap_materno = str(ap_materno)
    ap_materno = ap_materno.strip()
    ap_materno = ap_materno.upper()    
    
    # filtramos por varios campos
    persona = Persona.objects.filter(nombres=nombre, ap_paterno=ap_paterno, ap_materno=ap_materno).first()    
    # print(persona)    
    template_name = "bases/busqueda-nombre.html"
    context = {'obj': persona}    

    return render(request, template_name, context)

def get_personas_por_localidad(request):
    """ filtramos las personas por departamento""" 
    # obtenemos todos los departamentos para mostrar en el select
    localidades = Localidad.objects.all()
    
    # extraemos el valor del select
    localidad = request.POST.get("localidad") 
    # localidad = str(localidad)
    # localidad = localidad.strip()
    # localidad = localidad.upper()
    # print(localidad)
    # type(localidad)
    
    # filtramos todas las personas por departamento
    personas = Persona.objects.filter(localidad=localidad)
    # print(personas)
    
    template_name = "bases/filtrado-por-localidad.html"
    context = {
        'localidades': localidades,
        'personas': personas
    }   

    return render(request, template_name, context)

class MenuApoyos(LoginRequiredMixin, TemplateView):
    """ menú para búsquedas/filtros de apoyos"""
    template_name = "bases/menu-apoyos.html"
    login_url = 'bases:login' # sino esta logeado se redireccona al admin
    
def get_apoyos_por_departamento(request):
    """ filtramos los apoyos por departamento"""
    
    # mostramos en el select todos los departamentos
    departamentos = Departamento.objects.all()
    
    # extraemos el valor del select
    departamento = request.POST.get("departamento") 
    # muestra el id el departamento
    # print(departamento)
    # filtramos todos los apoyos por departamento
    apoyos = Apoyos.objects.filter(departamento=departamento)
    # print(apoyos)
    
    template_name = "bases/apoyos-por-departamento.html"
    context = {
        'departamentos': departamentos,
        'apoyos': apoyos
    }
    
    return render(request, template_name, context)

def get_apoyos_por_fecha(request):
    """ filtramos los apoyos por fecha"""
    # extraemos el valor del input fecha
    fecha_de_entrega = request.POST.get("fecha_de_entrega")
    print(fecha_de_entrega)
    print(type(fecha_de_entrega))
    
    # fecha_de_entrega = datetime.strptime(fecha_de_entrega, "%Y-%m-%d")
    # print(fecha)
        
    apoyos = Apoyos.objects.filter(fecha_de_entrega=fecha_de_entrega)
    print(apoyos)
    template_name = "bases/apoyos-por-fecha.html"
    context = {'apoyos': apoyos}
    
    return render(request, template_name, context)

def get_apoyos_por_localidad(request):
    """ filtramos los apoyos por localidad"""
    # mostramos en el select todos los departamentos
    localidades = Localidad.objects.all()
    
    # extraemos el valor del select
    localidad = request.POST.get("localidad") 
    # muestra el id de la localidad    
    #print(localidad)
    # filtramos todos los apoyos por localidad
    apoyos = Apoyos.objects.filter(localidad=localidad)
    # print(apoyos)
    
    template_name = "bases/apoyos-por-localidad.html"
    context = {
        'localidades': localidades,
        'apoyos': apoyos
    }
    
    return render(request, template_name, context)
    
    
    