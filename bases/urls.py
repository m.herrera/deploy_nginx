

from django.urls import path
from django.contrib.auth import views as auth_views

from bases.views import *

urlpatterns = [
    path('', Home.as_view(), name='home'),
    path('login/', auth_views.LoginView.as_view(template_name='bases/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='bases/login.html'), name='logout'),
    path('sin_privilegios/', HomeSinPrivilegios.as_view(), name="sin_privilegios"),
    path('bases/obtener-estadisticas/', getEstadisticas, name="obtener-estadisticas"),
        
    path('personas/menu', menu_de_personas, name='menu-personas'),
    # búsquede de persona por su curp
    path('personas/busqueda-por-curp', get_personas_por_curp, name='personas-curp'),
    
    # búsqueda de persona por nombre
    path('personas/busqueda-por-nombre', get_personas_por_nombre, name='bus_personas_por_nombre'),
    
    # filtramos las personas por departamento
    path('personas/personas-por-localidad', get_personas_por_localidad, name='personas_por_localidad'),
    
    # menu apoyos
    path('apoyos/menu', MenuApoyos.as_view(), name='menu_de_apoyos'),
    # filtrado de apoyos por departamento
    path('apoyos/apoyos-por-departamento', get_apoyos_por_departamento, name='apoyos_por_departamento'),
    # filtrado de apoyos por fecha
    path('apoyos/apoyos-por-fecha', get_apoyos_por_fecha, name="apoyos_por_fecha"),  
    # filtrado por localidad
    path('apoyos/apoyos-por-localidad', get_apoyos_por_localidad, name="apoyos_por_localidad"),  
]