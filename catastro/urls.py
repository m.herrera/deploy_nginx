
from django.urls import path
from .views import ContribuyenteView, ContribuyenteNew, ContribuyenteEdit, ContribuyenteDelete, \
    busqueda_por_cuenta_predial, obtener_contribuyente

urlpatterns = [
    # crud de contribuyentes
    path('contribuyentes/', ContribuyenteView.as_view(), name='contribuyentes_list'),
    path('contribuyentes/new', ContribuyenteNew.as_view(), name='contribuyentes_new'),
    path('contribuyentes/edit/<int:pk>', ContribuyenteEdit.as_view(), name='contribuyentes_edit'),
    path('contribuyentes/delete/<int:pk>', ContribuyenteDelete.as_view(), name='contribuyentes_delete'),
    
    # búsqueda de contribuyentes 
    path('contribuyentes/busqueda', busqueda_por_cuenta_predial, name='busqueda'),
    
    # búsqueda de contribuyentes por id
    path('contribuyentes/busqueda-id/<int:id>', obtener_contribuyente, name='obtener_contribuyente'),
    
    # path('contribuyentes/buscar/<int:id_cuenta>', busqueda_por_cuenta_predial, name='buscar_contribuyente'),
]